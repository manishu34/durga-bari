<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class WebController extends CI_Controller {


	public function index()
	{
		$this->load->view('home');
	}

	public function events_pg()
	{
		$this->load->view('events');
	}
	public function gallery_pg()
	{
		$this->load->view('gallery');
	}
	public function signup_pg()
	{
		$this->load->view('registration');
	}
}
