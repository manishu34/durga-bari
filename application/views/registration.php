<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/dist/home.css">
    <link rel="stylesheet" href="css/dist/gallery.css">
    <link rel="stylesheet" href="css/dist/signup.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">
    <title>Gallery | Durga Bari</title>

</head>
<body >
    <div class="wrapper">
        <?php include_once('header.php'); ?>
       <div class="banner">
           <h2>Signup & Celebrate</h2>
           <img class="design1" src="<?php echo base_url()?>images/illustrate.png" alt="">
           <img class="design2" src="<?php echo base_url()?>images/illustrate4.png" alt="">
       </div>
       
            
            
    <div class="main">
        <div class="title">
            <h2>Signup to be a part of the celebration</h2>
             <div class="illustDesign" >
                 <img src="<?php echo base_url()?>images/illustrate2.png" alt="" >
             </div>
        </div>


        <div class="signup_container">
            <div class="content_left">
                
                <div class="inner_content">
                    <div class="msg_box">
                        <p>When you will sign up, you will be able to</p>
                        <div class="arrow_down"></div>
                    </div>
                    <div class="title">
                        <h2>Upload your Pujo Photos</h2>
                         <div class="illustDesign" >
                             <img src="<?php echo base_url()?>images/illustrate2.png" alt="" >
                         </div>
                    </div>
                    <div class="title">
                        <h2>Post comments to Live Streams</h2>
                         <div class="illustDesign" >
                             <img src="<?php echo base_url()?>images/illustrate2.png" alt="" >
                         </div>
                    </div>
                    <div class="title">
                        <h2>Join Pujo Adda and share your mind</h2>
                         
                    </div>

                    <img src="<?php echo base_url()?>images/illustrate3.png" alt="" class="eye">

                <!-- inner_content end-->
                </div>
                
                

                <!-- content left end-->
            </div>
            <div class="content_right">
                <?php if($this->session->flashdata('failed')){ ?>
                <div class="alert alert-danger">
                    <p><?= $this->session->flashdata('failed'); ?></p>
                </div>
                <?php } ?>
                <?php if($this->session->flashdata('success')){ ?>
                <div class="alert alert-success">
                    <p><?= $this->session->flashdata('success'); ?></p>
                </div>
                <?php } ?>
                <div class="form_container">
                <?php echo validation_errors(); ?>
                    <form action="<?= base_url().'create-user'; ?>" method="POST">
                    <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">
                        <div class="form-group">
                            <?php echo form_input(array(
                                'type'=> 'text', 
                                'name' => 'name', 
                                'placeholder' => 'Your Name', 
                                'value'=> set_value('name'), 
                                'class' => 'form-control' 
                            )); ?>
                            <!-- <input type="text" name="name" class="form-control" placeholder="Your Name" value="<?php //echo set_value('name'); ?>"> -->
                            <?php
                                if($this->session->flashdata('username_error') != ''){
                                    echo '<small class="error">'.$this->session->flashdata('name_error').'</small>';
                                }
                                else{
                                    echo '';
                                }
                            ?>
                        </div>
                        <div class="form-group">
                            <input type="text" name="email" class="form-control" placeholder="Your email-id" value="<?php echo set_value('email'); ?>">
                            <?php
                                if($this->session->flashdata('username_error') != ''){
                                    echo '<small class="error">'.$this->session->flashdata('email_error').'</small>';
                                }
                                else{
                                    echo '';
                                }
                            ?>
                        </div>
                        <div class="form-group">
                            <input type="text" name="phone" class="form-control" placeholder="Your mobile phone no.">
                            <?php
                                if($this->session->flashdata('phone_error') != ''){
                                    echo '<small class="error">'.$this->session->flashdata('phone_error').'</small>';
                                }
                                else{
                                    echo '';
                                }
                            ?>
                        </div>
                        <div class="form-group">
                            
                            <input type="text" name="uname" class="form-control" placeholder="Username">
                            <?php
                                if($this->session->flashdata('username_error') != ''){
                                    echo '<small class="error">'.$this->session->flashdata('username_error').'</small>';
                                }
                                else{
                                    echo '';
                                }
                            ?>
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="Password" id="create_pass">
                            <?php
                                if($this->session->flashdata('password_error') != ''){
                                    echo '<small class="error">'.$this->session->flashdata('password_error').'</small>';
                                }
                                else{
                                    echo '';
                                }
                            ?>
                            <div class="pwd_error_length"></div>
                        </div>
                        <div class="form-group con_pwd">
                            <input type="password" name="con_password" class="form-control" placeholder="Confirm your password" id="confirm_pass">
                            <?php
                                if($this->session->flashdata('confirm_error') != ''){
                                    echo '<small class="error">'.$this->session->flashdata('confirm_error').'</small>';
                                }
                                else{
                                    echo '';
                                }
                            ?>
                            <div class="con_pwd_error_length"></div>
                        </div>
                        <div class="form-group check_bot">
                            <input type="checkbox" required>
                            I am not a robot
                        </div>
                        <div class="form-group">
                            <p>When you submit, you automatically agree to the Terms & Conditions set by organizing committee of Durga Bari Pujo for accessing and using the website.</p>
                        </div>
                        <div class="sub_btn">
                            <input type="submit" class="submit_btn " disabled value="Submit">
                            <div class="icon">
                                <img src="<?php echo base_url()?>images/submit.png" alt="">
                            </div>
                        </div>
                    </form>
                </div>
                

                <!-- content right end-->
            </div>

        </div>

        


        
        
        <!-- main end-->    
    </div>




        
        <?php include_once('footer.php'); ?>
    </div>




    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js "></script>

    <script>
        $(document).ready(function(){
            
            $('.con_pwd').hide();

            $('.small_device_nav_icon').on('click', function(e){
                e.preventDefault();
                $('html').addClass('scrollOff');
                $('.sidebar').addClass('active');
            })
            $('.close_sidebar').on('click', function(e){
                e.preventDefault();
                $('html').removeClass('scrollOff');
                $('.sidebar').removeClass('active');
            })

            var path = window.location;
            
            if( path == ''){
                path = 'index.html';
            }

            var target = $('#nav a[href="'+path+'"]');
            // var target = $('#nav a').attr('href') 
            // alert(target)
            target.parent().addClass('active');

        
            $('#create_pass').on('keyup', function(){
                
                var pwd = $('#create_pass').val().length;
                    if(pwd < 8){
                        $('.pwd_error_length').empty();
                        $('#create_pass').addClass('border_red');
                        $('#create_pass').removeClass('border_green');
                        $('.pwd_error_length').append('<small class="text-danger">Password should be 8 characters long.</small>')
                        $('.con_pwd').hide();
                    }
                    else{
                        $('.pwd_error_length').empty();
                        $('#create_pass').removeClass('border_red');
                        $('#create_pass').addClass('border_green');
                        $('.pwd_error_length').append('<small class="text-success">You have entered an strong Password.</small>');
                        $('.con_pwd').show();
                    }           
                
            })
            

            $('#confirm_pass').on('keyup', function(){
                
                var pass = $('#create_pass').val();
                var con_pwd = $('#confirm_pass').val();
                    
                    if(pass == con_pwd){
                        $('.con_pwd_error_length').empty();
                        $('#confirm_pass').addClass('border_green');
                        $('#confirm_pass').removeClass('border_red');
                        $('.con_pwd_error_length').append('<small class="text-success">Password Matched Successfully.</small>');
                        $(".submit_btn").prop('disabled', false);
                    }
                    else{
                        $('.con_pwd_error_length').empty();
                        $('#confirm_pass').removeClass('border_green');
                        $('#confirm_pass').addClass('border_red');
                        $('.con_pwd_error_length').append('<small class="text-danger">Password did not matched.</small>');
                        $(".submit_btn").prop('disabled', true);
                    }  
                    
                       
                
            })



        });
        


        
    </script>

</body>
</html>