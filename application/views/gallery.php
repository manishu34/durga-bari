<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?= base_url() ?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>css/dist/home.css">
    <link rel="stylesheet" href="<?= base_url() ?>css/dist/gallery.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">
    <link rel="stylesheet" href="<?= base_url() ?>css/mdb.min.css">
    <title>Gallery | Durga Bari</title>
</head>
<body >
    <div class="wrapper">
    <?php include_once('header.php'); ?>
    
       <div class="banner">
           <h2>Pujo Scope</h2>
           <img class="design1" src="<?= base_url() ?>images/illustrate.png" alt="">
           <img class="design2" src="<?= base_url() ?>images/illustrate4.png" alt="">
       </div>
       
            
            
    <div class="main">
        <div class="floating_btn">
            <div class="icon">
                <a href="#">
                    <i class="fa fa-camera" aria-hidden="true"></i>
                </a>
            </div>
            

            <div class="slideout">
                <p>Upload Photos</p>
            </div>
        </div>
       <div class="title">
           <h2>Photos Uploaded by Users All accross the globe</h2>

            <div class="illustDesign" >
                <img src="<?= base_url() ?>images/illustrate2.png" alt="" >
            </div>
       </div>
       
        <div class="gallery_container">
            <div class="gallery_date">
                <h3>20-October-2020</h3>
                <div class="pointer"></div>
            </div>
            <div class="gallery_row">
                <div class="gallery_col">
                   <div class="gallery_images">
                        <div class="image">
                            <img src="<?= base_url() ?>images/img1.jpg" class="img-fluid">
                            <div class="overlay">
                                <h4>@Somnath</h4>
                                <h3>04:35 PM</h3>
                                <a href="<?= base_url() ?>images/img1.jpg" data-toggle="lightbox" data-title="A random title" data-footer="A custom footer text">
                                    <img class="eye_icon" src="<?= base_url() ?>images/eye.png">
                                </a>
                            </div>
                        </div>
                        <div class="image">
                            <img src="<?= base_url() ?>images/img3.jpg" class="img-fluid">
                            <div class="overlay">
                                <h4>@Somnath</h4>
                                <h3>04:35 PM</h3>
                                <a href="<?= base_url() ?>images/img3.jpg" data-toggle="lightbox" data-title="A random title" data-footer="A custom footer text">
                                    <img class="eye_icon" src="<?= base_url() ?>images/eye.png">
                                </a>
                            </div>
                        </div>
                        <div class="image">
                            <img src="<?= base_url() ?>images/img3.jpg" class="img-fluid">
                            <div class="overlay">
                                <h4>@Somnath</h4>
                                <h3>04:35 PM</h3>
                                <a href="<?= base_url() ?>images/img3.jpg" data-toggle="lightbox" data-title="A random title" data-footer="A custom footer text">
                                    <img class="eye_icon" src="<?= base_url() ?>images/eye.png">
                                </a>
                            </div>
                        </div>
                        <div class="image">
                            <img src="<?= base_url() ?>images/img2.jpg" class="img-fluid">
                            <div class="overlay">
                                <h4>@Somnath</h4>
                                <h3>04:35 PM</h3>
                                <a href="<?= base_url() ?>images/img2.jpg" data-toggle="lightbox" data-title="A random title" data-footer="A custom footer text">
                                    <img class="eye_icon" src="<?= base_url() ?>images/eye.png">
                                </a>
                            </div>
                        </div>
                        <div class="image">
                            <img src="<?= base_url() ?>images/img3.jpg" class="img-fluid">
                            <div class="overlay">
                                <h4>@Somnath</h4>
                                <h3>04:35 PM</h3>
                                <a href="<?= base_url() ?>images/img3.jpg" data-toggle="lightbox" data-title="A random title" data-footer="A custom footer text">
                                    <img class="eye_icon" src="<?= base_url() ?>images/eye.png">
                                </a>
                            </div>
                        </div>
                        <div class="image">
                            <img src="<?= base_url() ?>images/img1.jpg" class="img-fluid">
                            <div class="overlay">
                                <h4>@Somnath</h4>
                                <h3>04:35 PM</h3>
                                <a href="<?= base_url() ?>images/img1.jpg" data-toggle="lightbox" data-title="A random title" data-footer="A custom footer text">
                                    <img class="eye_icon" src="<?= base_url() ?>images/eye.png">
                                </a>
                            </div>
                        </div>
                        <div class="image">
                            <img src="<?= base_url() ?>images/img1.jpg" class="img-fluid">
                            <div class="overlay">
                                <h4>@Somnath</h4>
                                <h3>04:35 PM</h3>
                                <a href="<?= base_url() ?>images/img1.jpg" data-toggle="lightbox" data-title="A random title" data-footer="A custom footer text">
                                    <img class="eye_icon" src="<?= base_url() ?>images/eye.png">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="gallery_container">
            <div class="gallery_date">
                <h3>21-October-2020</h3>
                <div class="pointer"></div>
            </div>
            <div class="gallery_row">
                <div class="gallery_col">
                   <div class="gallery_images">
                        <div class="image">
                            <img src="<?= base_url() ?>images/img1.jpg" class="img-fluid">
                            <div class="overlay">
                                <h4>@Somnath</h4>
                                <h3>04:35 PM</h3>
                                <a href="<?= base_url() ?>images/img1.jpg" data-toggle="lightbox" data-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, quaerat!" data-footer="A custom footer text">
                                    <img class="eye_icon" src="<?= base_url() ?>images/eye.png">
                                </a>
                            </div>
                        </div>
                        <div class="image">
                            <img src="<?= base_url() ?>images/img3.jpg" class="img-fluid">
                            <div class="overlay">
                                <h4>@Somnath</h4>
                                <h3>04:35 PM</h3>
                                <a href="<?= base_url() ?>images/img3.jpg" data-toggle="lightbox" data-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, quaerat!" data-footer="A custom footer text">
                                    <img class="eye_icon" src="<?= base_url() ?>images/eye.png">
                                </a>
                            </div>
                        </div>
                        <div class="image">
                            <img src="<?= base_url() ?>images/img3.jpg" class="img-fluid">
                            <div class="overlay">
                                <h4>@Somnath</h4>
                                <h3>04:35 PM</h3>
                                <a href="<?= base_url() ?>images/img3.jpg" data-toggle="lightbox" data-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, quaerat!" data-footer="A custom footer text">
                                    <img class="eye_icon" src="<?= base_url() ?>images/eye.png">
                                </a>
                            </div>
                        </div>
                        <div class="image">
                            <img src="<?= base_url() ?>images/img2.jpg" class="img-fluid">
                            <div class="overlay">
                                <h4>@Somnath</h4>
                                <h3>04:35 PM</h3>
                                <a href="<?= base_url() ?>images/img2.jpg" data-toggle="lightbox" data-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, quaerat!" data-footer="A custom footer text">
                                    <img class="eye_icon" src="<?= base_url() ?>images/eye.png">
                                </a>
                            </div>
                        </div>
                        <div class="image">
                            <img src="<?= base_url() ?>images/img3.jpg" class="img-fluid">
                            <div class="overlay">
                                <h4>@Somnath</h4>
                                <h3>04:35 PM</h3>
                                <a href="<?= base_url() ?>images/img3.jpg" data-toggle="lightbox" data-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, quaerat!" data-footer="A custom footer text">
                                    <img class="eye_icon" src="<?= base_url() ?>images/eye.png">
                                </a>
                            </div>
                        </div>
                        <div class="image">
                            <img src="<?= base_url() ?>images/img1.jpg" class="img-fluid">
                            <div class="overlay">
                                <h4>@Somnath</h4>
                                <h3>04:35 PM</h3>
                                <a href="<?= base_url() ?>images/img1.jpg" data-toggle="lightbox" data-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, quaerat!" data-footer="A custom footer text">
                                    <img class="eye_icon" src="<?= base_url() ?>images/eye.png">
                                </a>
                            </div>
                        </div>
                        <div class="image">
                            <img src="<?= base_url() ?>images/img4.jpg" class="img-fluid">
                            <div class="overlay">
                                <h4>@Somnath</h4>
                                <h3>04:35 PM</h3>
                                <a href="<?= base_url() ?>images/img4.jpg" data-toggle="lightbox" data-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, quaerat!" data-footer="A custom footer text">
                                    <img class="eye_icon" src="<?= base_url() ?>images/eye.png">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        


        
        

    </div>




        
        <?php include_once('footer.php'); ?>
    </div>




    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="<?= base_url() ?>js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js "></script>

    <script>
        $(document).ready(function(){
            


            $('.small_device_nav_icon').on('click', function(e){
                e.preventDefault();
                $('html').addClass('scrollOff');
                $('.sidebar').addClass('active');
            })
            $('.close_sidebar').on('click', function(e){
                e.preventDefault();
                $('html').removeClass('scrollOff');
                $('.sidebar').removeClass('active');
            })

            var path = window.location;
            
            if( path == ''){
                path = 'index.html';
            }

            var target = $('#nav a[href="'+path+'"]');
            // var target = $('#nav a').attr('href') 
            // alert(target)
            target.parent().addClass('active');

        });
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });


        
    </script>

</body>
</html>