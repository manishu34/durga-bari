<div class="sidebar">
            <div class="close_sidebar">
                <i class="fa fa-times" aria-hidden="true"></i>
            </div>
            <ul id="nav">
                <li>
                    <a href="<?php echo base_url().'' ?>">Home</a>
                </li>
                <li>
                    <a href="<?php echo base_url().'live-events'?>">Live Events</a>
                </li>
                <li>
                    <a href="<?php echo base_url().'pujo-gallery' ?>">Gallery</a>
                </li>
                <li>
                    <a href="#">Pujo Adda</a>
                </li>
                <li>
                    <a href="#">Contact Us</a>
                </li>
                <li>
                    <a href="<?php echo base_url().'create-account' ?>">Signup</a>
                </li>
            </ul>
        </div>
        <div class="header">
            <div class="navbar_wrapper">
                <div class="navbar_logo">
                    <div class="logo_wrap">
                        <img src="images/durgabari2.png" alt="">
                    </div>
                </div>
                <div class="navbar_menu">

                    <ul id="nav">
                    <li>
                            <a href="<?php echo base_url().'' ?>">Home</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url().'live-events' ?>">Event & Live</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url().'pujo-gallery' ?>">Gallery</a>
                        </li>
                        <li>
                            <a href="#">Pujo Adda</a>
                        </li>
                        <li>
                            <a href="#">Contact Us</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url().'create-account' ?>">Signup</a>
                        </li>
                    </ul>
                    <div class="small_device_nav_icon">
                        <i class="fa fa-bars" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>